<?php


namespace App\Classe;


use App\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class Cart
{
    private $session;
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager,SessionInterface $session) {
        $this->session = $session;
        $this->entityManager = $entityManager;
    }

    public function add($product) {
        $cart = $this->session->get('cart' , [
            'products' => [],
            'annotations' => ['success' => '', 'error' => '']
        ]);

        $id = (int) $product['id'];
        $duree = (int)  $product['duree'];
        $nb_km = (int) $product['nb_km'];

        $product = $this->entityManager->getRepository(Product::class)->findOneById($id);

        //dd($product->getIsAvailable());
        if(!$product->getIsAvailable()) {
            unset($cart['annotations']['success']);
            $cart['annotations']['error'] = "Ce véhicule n'est pas diponible";
        } else {
            if(empty($cart['products'][$id])) {
                $cart['products'][$id] = [
                    'duree' => $duree,
                    'nb_km' => $nb_km
                ];
                unset($cart['annotations']['error']);
                $cart['annotations']['success'] = "Votre location a été ajouté a votre panier avec succès !";
            } else {
                //dd('deux fois');
                unset($cart['annotations']['success']);
                $cart['annotations']['error'] = "Vous ne pouvez pas avoir deux fois le même véhicule dans le panier !";
            }
        }
        //dd($cart);




        $this->session->set('cart', $cart);
    }

    public function get() {
            return $this->session->get('cart');
    }

    public function remove() {
        return $this->session->remove('cart');
    }

    public function delete($id) {

        $cart = $this->get();

        unset($cart['products'][(int)$id]);

        //dd($cart);
        $cart['annotations']['success'] = "Votre véhicule a été supprimmée avec succèes";

        return $this->session->set('cart', $cart);
    }

    public function getFull() {
        $cartComplete = [];

        //dd($this->get('cart'));

        if(isset($this->get()['products'])) {
            foreach ($this->get()['products'] as $id => $value) {
                $product_obj = $this->entityManager->getRepository(Product::class)->findOneById($id);

                if(!$product_obj) {
                    $this->delete($id);
                    continue;
                }
                    $cartComplete['products'][$id] = [
                        'product' => $this->entityManager->getRepository(Product::class)->findOneById($id),
                        'duree' => $value['duree'],
                        'nb_km' => $value['nb_km'],
                        'price' => $value['duree'] * $value['nb_km'],
                    ];
            }
            $cartComplete['annotations'] = $this->get('cart')['annotations'];
        }

        return $cartComplete;
    }

    public function increase($id, $item) {

        $cart = $this->get();
        $cart['products'][$id][$item] += 1;

        return $this->session->set('cart', $cart);

    }

    public function decrease($id, $item) {

        $cart = $this->get();

        if($cart['products'][$id][$item] > 1) {
            $cart['products'][$id][$item] -= 1;
        } 

        return $this->session->set('cart', $cart);

    }
}