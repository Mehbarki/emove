<?php

namespace App\Controller;

use App\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    /**
     * @Route("/nous-contacter", name="contact")
     */
    public function index(Request $request)
    {

        $form = $this->createForm(ContactType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $this->addFlash('notice', 'Votre message a était envoyé ! nous allons l\'étudier dans les plus bref delais
             afin de vous répondre rapidement.');
        }



        return $this->render('contact/index.html.twig', [
            'form' => $form->createView()

            ]);
    }
}
