<?php

namespace App\Controller;

use App\Classe\Search;
use App\Entity\Product;
use App\Form\SearchType;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager) {
        $this->entityManager = $entityManager;
    }
    /**
     * @Route("/nos-produits", name="products")
     */
    public function index(Request $request): Response
    {

        $search = new Search();
        $form = $this->createForm(SearchType::class, $search);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //dd($search);
            $products = $this->entityManager->getRepository(Product::class)->findWithSearch($search);
        } else {
            $products = $this->entityManager->getRepository(Product::class)->findAll();
        }

        return $this->render('product/index.html.twig', [
            'products' => $products,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/produit/{slug}", name="product")
     */
    public function show(Request $request, $slug): Response
    {

        $product = $this->entityManager->getRepository(Product::class)->findOneBySlug($slug);
        $products = $this->entityManager->getRepository(Product::class)->findByIsBest(1);

        if(!$product) {
            return $this->redirectToRoute('products');
        }

        $defaultData = ['id' => $product->getId()];
        $form = $this->createFormBuilder($defaultData)
            ->add('duree', IntegerType::class, [
                'label' => 'La durée de location (en jours)',
                'attr' => [
                    'placeholder' => '3'
                ],
                'required' => true,
            ])
            ->add('nb_km', IntegerType::class, [
                'label' => 'Le nombre de kilomètres de la location (en km)',
                'attr' => [
                    'placeholder' => '250'
                ],
                'required' => true,


            ])
            ->add('submit', SubmitType::class, [
                'label' => "Ajouter au panier"
            ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // data is an array with "name", "email", and "message" keys
            $data = $form->getData();
            //dd($data);

            return $this->redirectToRoute('add_to_cart',array('product' => $data));
        }

        return $this->render('product/show.html.twig', [
            'product' => $product,
            'products' => $products,
            'form' => $form->createView()
        ]);
    }
}
