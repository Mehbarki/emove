<?php

namespace App\Controller;

use App\Entity\OrderDetails;
use App\Entity\Product;
use App\Classe\Cart;
use App\Form\OrderType;
use App\Entity\Order;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;
use Knp\Snappy\Pdf;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends AbstractController
{
    private $entityManager;
    private $pdf;

    public function __construct(EntityManagerInterface $entityManager, Pdf $pdf)
    {
        $this->entityManager = $entityManager;
        $this->pdf = $pdf;
    }

    /**
     * @Route("/commande", name="order")
     */
    public function index(Cart $cart, Request $request): Response
    {
        if (!$this->getUser()->getAddresses()->getValues()) {
            return $this->redirectToRoute('account_address_add');
        }

        $form = $this->createForm(OrderType::class, null, [
            'user' => $this->getUser(),
        ]);

        return $this->render('order/index.html.twig', [
            'form' => $form->createView(),
            'cart' => $cart->getFull(),
        ]);
    }

    /**
     * @Route("/commande/recapitulatif", name="order_recap", methods = {"POST"})
     */
    public function add(Cart $cart, Request $request): Response
    {

        $form = $this->createForm(OrderType::class, null, [
            'user' => $this->getUser(),
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $date = new \DateTime();
            $carriers = $form->get('carriers')->getData();
            $delivery = $form->get('addresses')->getData();
            $delivery_content = $delivery->getFirstName() . ' ' . $delivery->getLastName();
            $delivery_content .= '<br/>' . $delivery->getPhone();

            if ($delivery->getCompany()) {
                $delivery_content .= '<br/>' . $delivery->getCompany();
            }

            $delivery_content .= '<br/>' . $delivery->getAddress();
            $delivery_content .= '<br/>' . $delivery->getPostal() . ' ' . $delivery->getCity();
            $delivery_content .= '<br/>' . $delivery->getCountry();

            //dd($delivery_content);

            //Enregister ma commande
            $order = new Order();
            $order->setUser($this->getUser());
            $order->setCreatedAt($date);
            $order->setCarrierName($carriers->getName());
            $order->setCarrierPrice($carriers->getPrice());
            $order->setDelivery($delivery_content);
            $order->setInProgress(0);

            $this->entityManager->persist($order);

            //Enregister mes produits
            foreach ($cart->getFull()['products'] as $product) {
                //dd($product);
                $orderDetails = new OrderDetails();
                $orderDetails->setMyOrder($order);
                $orderDetails->setProduct($product['product']->getMarque());
                $orderDetails->setNbKm($product['nb_km']);
                $orderDetails->setDuree($product['duree']);
                $orderDetails->setPrice($product['price']);
                $orderDetails->setIdProduct($product['product']->getId());

                $interval = new \DateInterval('P' . $product['duree'] . 'D');
                $date = new \DateTime(null, new \DateTimeZone('Europe/Paris'));
                $expireAt = $date->add($interval);

                $orderDetails->setExpireAt($expireAt);

                $this->entityManager->persist($orderDetails);
            }
            $this->entityManager->flush();
            return $this->render('order/add.html.twig', [
                'cart' => $cart->getFull(),
                'carrier' => $carriers,
                'delivery' => $delivery_content
            ]);
        } else {
            return $this->redirectToRoute('cart');
        }
    }

    /**
     * @Route("/commande/success/", name="order_success")
     */

    //MAIL SI VEHICULES PAS RENDU DANS LES TEMPS
    //FACTURE QUAND VEHICULE RENDU
    //CONTRAT DE LOCATION QUAND VALIDE LA COMMANDE ENVOYE PAR MAIL


    public function success(Cart $cart): Response
    {

        $order = $this->entityManager->getRepository(Order::class)->findOneBy(
            ['user' => $this->getUser()->getId()],
            ['id' => 'DESC']
        );
        $order->setInProgress(1);

        $orders = $order->getOrderDetails()->getValues();
        //dd($orders);

        foreach ($orders as $order) {
            $product = $this->entityManager->getRepository(Product::class)->findOneById($order->getIdProduct());
            $product->setIsAvailable(0);
            //dd($product);
        }

        $this->entityManager->flush();

        $cart->remove();

        return $this->render('order/success.html.twig');

    }

    /**
     * @Route("/commande/contract/", name="order_contract")
     */

    public function createContractPdf()
    {

        $order = $this->entityManager->getRepository(Order::class)->findOneBy(
            ['user' => $this->getUser()->getId()],
            ['id' => 'DESC']
        );

        $products = $order->getOrderDetails()->getValues();
        $carrier = ['name' => $order->getCarrierName(), 'price' => $order->getCarrierPrice()];
//dd($products);
        $html = $this->renderView('order/contract.html.twig', array(
                'carrier' => $carrier,
                'delivery' => $order->getDelivery(),
                'products' => $products
            )
        );

        return new PdfResponse(
            $this->pdf->getOutputFromHtml($html, ['encoding' => 'UTF-8']),
            sprintf('contrat_de_location_%s.pdf', date('Y-m-d'))
        );

    }

    /**
     * @Route("/commande/contract/{id}", name="order_contract_one")
     */

    public function createOneContractPdf($id)
    {

        if($id) {

            $order = $this->entityManager->getRepository(Order::class)->findOneBy(
                [
                    'user' => $this->getUser()->getId(),
                    'id' => $id
                ],
            );

            //dd($order);

            $products = $order->getOrderDetails()->getValues();
            $carrier = ['name' => $order->getCarrierName(), 'price' => $order->getCarrierPrice()];

            //dd($products);

            $html = $this->renderView('order/contract.html.twig', array(
                    'carrier' => $carrier,
                    'delivery' => $order->getDelivery(),
                    'products' => $products
                )
            );

            return new PdfResponse(
                $this->pdf->getOutputFromHtml($html, ['encoding' => 'UTF-8']),
                sprintf('contrat_de_location_%s.pdf', date('Y-m-d'))
            );
        }
    }

    /**
     * @Route("/commande/historique/", name="order_history")
     */

    public function show()
    {

        $orders['inProgress'] = $this->entityManager->getRepository(Order::class)->findBy(
            [
                'user' => $this->getUser()->getId(),
                'inProgress' => 1
            ],
        );

        $orders['done'] = $this->entityManager->getRepository(Order::class)->findBy(
            [
                'user' => $this->getUser()->getId(),
                'inProgress' => 0
            ],
        );

        //dd($orders);

        return $this->render('order/show.html.twig', [
            'orders' => $orders
        ]);

    }


    /**
     * @Route("/commande/historique/{id}", name="order_history_details")
     */

    public function showOne($id)
    {

        $order = $this->entityManager->getRepository(Order::class)->findOneBy(
            [
                'user' => $this->getUser()->getId(),
                'inProgress' => 1,
                'id' => $id
            ]
        );
        if($order) {

            $orders = $order->getOrderDetails()->getValues();
            foreach ($orders as $order_product) {
                $products[] = $this->entityManager->getRepository(Product::class)->findOneById($order_product->getIdProduct());
            }

            //dd($orders);

            return $this->render('order/showOne.html.twig', [
                'products' => $products,
                'orders' => $orders
            ]);
        } else {
            return $this->redirectToRoute('order_history');
        }


    }

    /**
     * @Route("/commande/render/{idProduct}/{idOrderDetails}", name="order_render_product")
     */

    public function renderOne($idProduct, $idOrderDetails)
    {
        if($idProduct && $idOrderDetails) {

            $date = new \DateTime(null, new \DateTimeZone('Europe/Paris'));
            $order_detail = $this->entityManager->getRepository(OrderDetails::class)->findOneById($idOrderDetails);
            $order_detail->setRenderOn($date);

            $product = $this->entityManager->getRepository(Product::class)->findOneById($idProduct);
            $product->setIsAvailable(1);

            $id_order = $order_detail->getMyOrder()->getId();
            $order = $this->entityManager->getRepository(OrderDetails::class)->findBy([
                'myOrder' => $id_order
            ]);

            $endLocation = true;

           foreach ($order as $product) {
               if($product->getRenderOn() === null) {
                   $endLocation = false;
               }
           }

           if($endLocation) {
                $order = $this->entityManager->getRepository(Order::class)->findOneById($id_order);
                $order->setInProgress(0);
           }

           //dd($endLocation);

            $this->entityManager->flush();
            return $this->redirectToRoute('order_history_details', ['id' => $id_order]);

        }




    }
}
