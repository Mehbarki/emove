<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegisterType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegisterController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager) {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/inscription", name="register")
     */
    public function index(Request $request, UserPasswordHasherInterface $hasher): Response
    {
        $user = new User();
        $form = $this->createForm(RegisterType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            //dd($form->getData()->getEmail());
            $user = $form->getData();

            $user_exist = $this->entityManager->getRepository(User::class)->findOneBy([
                'email' => $form->getData()->getEmail()
                ]);

            if($user_exist === null) {
                $password = $hasher->hashPassword($user,  $user->getPassword());

                $user->setPassword($password);

                $this->entityManager->persist($user);
                $this->entityManager->flush();

                return $this->redirectToRoute('app_login');
            } else {
                $error = "L'email est déjà utilisé, veuillez en choisir un autre !";
                return $this->render('register/index.html.twig', [
                    "form" => $form->createView(),
                    'error' => $error]);
            }

            dd($user_exist);


        }


        return $this->render('register/index.html.twig', [
            "form" => $form->createView()]);
    }
}
